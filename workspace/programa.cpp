#include <stdlib.h>
#include <iostream>
using namespace std;

#include "matriz_grafo.h"


/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "============= Menu =============" << endl;
    cout << "Aplicar algoritmo Dijkstra _ [1]" << endl;
    cout << "Mostrar matriz _____________ [2]" << endl;
    cout << "Visualizar el grafo ________ [3]" << endl;
    cout << "Salir del programa _________ [0]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> opt;

    clear();

    return opt;
}

/* Función para el preguntar dato al usuario */
string solicitar_dato (string dato, string enunciado) {
    cout << "======== " << enunciado << " ========" << endl;
    cout << "Ingresar: ";
    cin >> dato;

    clear();

    return dato;
}


/* Función main */
int main(int argc, char **argv){

    // Valida cantidad de parámetros mínimos.
    if (argc<2) {
		cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl;
		cout << "-> Ejemplo: ./programa 'Número mayor a 2' (sin comillas)" << endl;
    }

    // Convierte string a entero && tamano_matriz es una variable int para crear la matriz NxN.
    int tamano_matriz = atoi(argv[1]);

    // Inicializa las variables para el menu
    string option = "\0";
    string dato = "\0";

    // Crea el objeto dijkstra
    matriz_grafo *dijkstra = new matriz_grafo();

    /*
    Funciones iniciales: Generar matriz
    cout << "Ingrese el tamaño de la matriz: ";
    cin >> tamano_matriz;
    */

    // Inicializa un Array del tipo string con nombres de los nodos
    string nombres[tamano_matriz];

    // Genera la matriz como atributo del objeto dijkstra junto a los nombres y el tamaño
    dijkstra->agregar_nombres(tamano_matriz, nombres);
    dijkstra->llenar_matriz(tamano_matriz, nombres);

    clear();

    while (option != "0") {

        option = menu_principal(option);

        // Opción para ver los caminos más cortos de un nodo específico
        if (option == "1") {
            dato = solicitar_dato(dato,"Nodo objetivo");
            dijkstra->dijkstra(tamano_matriz,dato,nombres);
        
        // Opción para mostrar la matriz
        }else if (option == "2") {
            dijkstra->mostrar_matriz(tamano_matriz);

        //Opción para crear y visualizar el grafo con la librería graphviz
        }else if (option == "3") {
            dijkstra->crear_grafo(tamano_matriz, nombres);

        }
    }

    return 0;
}