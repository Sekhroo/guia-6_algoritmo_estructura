#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;

#define TRUE 0
#define FALSE 1

#include "matriz_grafo.h"

matriz_grafo::matriz_grafo(){}

/* Función para asignar nombres a los nodos */
void matriz_grafo::agregar_nombres(int total_nodos, string nombres[]) {
    string nombre;

    for (int i=0;i<total_nodos;i++) {
        cout << "\n";
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

/* Función para rellenar la matriz segun lo que indique el usuario */
void matriz_grafo::llenar_matriz(int total_nodos, string nombres[]) {
    this->matriz = new int*[total_nodos];
    int distancia;

    /* Inicializamos la matriz */
    for (int i=0;i<total_nodos;i++) {
        this->matriz[i] = new int[total_nodos];
    }

    /* Llenar la matriz */
    for (int i=0;i<total_nodos;i++) {
        cout << "\n";
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << "\n";

        for (int j=0; j<total_nodos;j++) {
            if (i != j) {
                cout << "ingrese la distancia al nodo <" << nombres[j] << ">: ";
                cin >> distancia;

                this->matriz[i][j] = distancia;
            } else {
                /* Diagonal de la matriz */
                this->matriz[i][j] = 0;
            } 
        }
    }
}

/* Función para mostrar la matriz */
void matriz_grafo::mostrar_matriz(int total_nodos) {
    for (int i=0; i<total_nodos;i++) {
        for (int j=0;j<total_nodos;j++) {
            cout << this->matriz[i][j] << " ";
        }
        cout << "\n";
    }
}

/* Función para encontrar el indice de la matriz */
int matriz_grafo::buscar_matriz(int total_nodos, string dato, string nombres[]) {
    for (int i=0; i<total_nodos;i++) {
        if (nombres[i] == dato) {
            return i;
        }
    }
}

/* Función para generar la imagen del grafo  */
void matriz_grafo::crear_grafo(int total_nodos, string nombres[]) {

    ofstream fp;
            
    /* Abre archivo */
    fp.open ("grafo.txt");
    fp << "digraph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
    
    // Escribir en el archivo la matriz
    for (int i=0; i<total_nodos; i++) {
        for (int j=0; j<total_nodos; j++) {
            // evalua la diagonal principal.
            if (i != j) {
                if (matriz[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "->" << '"' << nombres[j] << '"' << " [label=" << this->matriz[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    /* Cierra archivo */
    fp.close();
                    
    /* Genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
    /* Visualiza el grafo */ 
    system("eog grafo.png &");
}



//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::  Implimentación del código de apoyo <<conversión C a C++>> ::::::://
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://

// Función central que aplica el algoritmo de Dijkstra
void matriz_grafo::dijkstra(int total_nodos, string dato, string nombres[]){
    // Inicializa los vectores con el tamaño indicado
    char V[total_nodos], S[total_nodos], VS[total_nodos];
    int D[total_nodos];

    // Rellena los vectores
    inicializar_vector_caracter(V, total_nodos);
    inicializar_vector_caracter(S, total_nodos);
    inicializar_vector_caracter(VS, total_nodos);
  
    // Inicializa el vertice
    leer_nodos(V, total_nodos);

    // int directriz = buscar_matriz(total_nodos, dato, nombres);
    
    // Aplica el algoritmo de dijkstra --- Se utiliza la función buscar matriz para obtener el indice buscado
    aplicar_dijkstra (V, S, VS, D, total_nodos, buscar_matriz(total_nodos, dato, nombres), nombres);
}

// Copia contenido inicial a D[] desde la matriz M[][].
void matriz_grafo::inicializar_vector_D (int D[], int total_nodos, int directriz) {
    
    // Utiliza la directriz como indice del nodo seleccionado
    for (int col=0; col<total_nodos; col++) {
        D[col] = this->matriz[directriz][col];
    }
}

// Inicializa con espacios el arreglo de caracteres.
void matriz_grafo::inicializar_vector_caracter (char vector[], int total_nodos) {
  
    for (int col=0; col<total_nodos; col++) {
        vector[col] = ' ';
    }
}

// Aplica el algoritmo.
void matriz_grafo::aplicar_dijkstra (char V[], char S[], char VS[], int D[], int total_nodos, int directriz, string nombres[]) {
    int v;

    // Inicializar vector D[] segun datos de la matriz M[][] 
    // Estado inicial.
    inicializar_vector_D(D, total_nodos, directriz);

    // Visualizar en la terminal los estados iniciales
    cout << "---------Estados iniciales ---------------------------------------" << endl;
    cout << "Matriz: ";
    mostrar_matriz(total_nodos);
    cout << endl;
    imprimir_vector_caracter(S, "S", total_nodos);
    imprimir_vector_caracter(VS, "VS", total_nodos);
    imprimir_vector_entero(D, total_nodos);
    cout << "------------------------------------------------------------------" << endl << endl;
  
    // Agrega el vertice escogido.
    cout << "> Agregar ["<< nombres[directriz] <<"] que equivale a V["<< directriz <<"] a S[] y actualiza VS[]" << endl << endl;;
    agrega_vertice_a_S (S, V[directriz], total_nodos);

    // Imprime el vector para visualizar paso a paso en la terminal el progreso del algoritmo
    imprimir_vector_caracter(S, "S", total_nodos);
    
    // Actualiza los vectores buscando espacios
    actualizar_VS (V, S, VS, total_nodos);

    // Imprime los vectores para visualizar paso a paso en la terminal el progreso del algoritmo
    imprimir_vector_caracter(VS, "VS", total_nodos);
    imprimir_vector_entero(D, total_nodos);

    for (int i=1; i<total_nodos; i++) {
        // Elige un vértice en v de VS[] tal que D[v] sea el mínimo 
        cout << "\n> elige vertice menor en VS[] según valores en D[]\n";
        cout << "> lo agrega a S[] y actualiza VS[]\n";
        v = elegir_vertice (VS, D, V, total_nodos);

        // Agrega los vertices menores y los imprime
        agrega_vertice_a_S (S, v, total_nodos);
        imprimir_vector_caracter(S, "S", total_nodos);

        // Actualiza los vectores buscando espacios
        actualizar_VS (V, S, VS, total_nodos);
        imprimir_vector_caracter(VS, "VS", total_nodos);

        // Actualiza los vectores buscando minumos e imprimiendolos
        actualizar_pesos(D, VS, V, v, total_nodos);
        imprimir_vector_entero(D, total_nodos);
            
    }
    cout << endl << endl;
}

// Actualizar vector buscando minimos  e indices para generar el vector D
void matriz_grafo::actualizar_pesos (int D[], char VS[], char V[], char v, int total_nodos) {
    int i = 0;
    int indice_w, indice_v;

    cout << "\n> actualiza pesos en D[]\n";
  
    indice_v = buscar_indice_caracter(V, v, total_nodos);
    while (VS[i] != ' ') {
        if (VS[i] != v) {
            indice_w = buscar_indice_caracter(V, VS[i], total_nodos);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], this->matriz[indice_v][indice_w]);
        }
        i++;
    }
}

// Calcular valor minimo
int matriz_grafo::calcular_minimo(int dw, int dv, int mvw) {
    int min = 0;

  //
    if (dw == -1) {
        if (dv != -1 && mvw != -1)
            min = dv + mvw;
        else
            min = -1;

    } else {
        if (dv != -1 && mvw != -1) {
            if (dw <= (dv + mvw))
                min = dw;
            else
                min = (dv + mvw);
        }
        else
            min = dw;
        }
  
    cout << "dw: " << dw << " dv: " << dv << " mvw: " << mvw << " min: " << min << "\n";

    return min;
}

// Agrega vértice a S[].
void matriz_grafo::agrega_vertice_a_S(char S[], char vertice, int total_nodos) {
  
    // Recorre buscando un espacio vacio.
    for (int i=0; i<total_nodos; i++) {
        if (S[i] == ' ') {
            S[i] = vertice;
            return;
        }
    }  
}

// Elige vértice con menor peso en VS[]  <======> busca su peso en D[].
int matriz_grafo::elegir_vertice(char VS[], int D[], char V[], int total_nodos) {
    int i = 0;
    int menor = 0;
    int peso;
    int vertice;

    while (VS[i] != ' ') {
        peso = D[buscar_indice_caracter(V, VS[i], total_nodos)];
        // Descarta valores infinitos (-1) y 0.
        if ((peso != -1) && (peso != 0)) {
            if (i == 0) {
                menor = peso;
                vertice = VS[i];
            } else {
                if (peso < menor) {
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }

    i++;
    }
  
    cout << endl << "vertice: " << vertice << endl << endl;
    return vertice;
}

// Retorna el índice del caracter consultado.
int matriz_grafo::buscar_indice_caracter(char V[], char caracter, int total_nodos) {
    int i = 0;

    for (i=0; i<total_nodos; i++) {
        if (V[i] == caracter){
            return i;
        }
    }
  
    return i;
}

// Busca la aparición de un caracter en un vector de caracteres.
int matriz_grafo::busca_caracter(char c, char vector[], int total_nodos) {
  
    for (int j=0; j<total_nodos; j++) {
        if (c == vector[j]) {
        return TRUE;
        }
    }
  
    return FALSE;
}

// Actualiza VS[] cada ves que se agrega un elemnto a S[].
void matriz_grafo::actualizar_VS(char V[], char S[], char VS[], int total_nodos) {
    int k = 0;
  
    inicializar_vector_caracter(VS, total_nodos);
  
    for (int j=0; j<total_nodos; j++){
        // Por cada caracter de V[] evalua si está en S[],
        // Sino está, lo agrega a VS[].
        if (busca_caracter(V[j], S, total_nodos) != TRUE) {
            VS[k] = V[j];
            k++;
        }
    }
}

// Lee datos de los nodos <==============> Inicializa utilizando código ASCII.
void matriz_grafo::leer_nodos (char vector[], int total_nodos) {
    int inicio = 97;

    // Recorre el vertor inicializando
    for (int i=0; i<total_nodos; i++) {
        vector[i] = inicio+i;
    }
}

// Imprime el contenido de un vector de caracteres.
void matriz_grafo::imprimir_vector_caracter(char vector[], char *nomVector, int total_nodos) {
  
    for (int i=0; i<total_nodos; i++) {
        cout << "| " << nomVector << "[" << i << "]: "<<vector[i] << " |";
    }
    cout << "\n";
}

// Imprimir vectores tipo int
void matriz_grafo::imprimir_vector_entero(int vector[], int total_nodos) {

    // Recorrer
    for (int i=0; i<total_nodos; i++) {
        cout << "| " << "D[" << i <<"]: "<< vector[i] << " |";
    }
    cout << "\n";
}