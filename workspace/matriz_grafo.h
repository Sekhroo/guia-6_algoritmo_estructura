#ifndef MATRIZ_GRAFO_H
#define MATRIZ_GRAFO_H

#include <stdlib.h>
#include <iostream>
using namespace std;

class matriz_grafo {

    private:
        // Atributo del objeto: Matriz
        int **matriz;
        

    public:
        matriz_grafo();

        /* Funciones de soporte para balancear el árbol */
        void agregar_nombres(int total_nodos, string nombres[]);

        /* Funciones para restructurar el árbol en caso de desbalanceo*/
        void llenar_matriz(int total_nodos, string nombres[]);

        /* Funciones para crear e insertar nuevos nodos al árbol */
        void mostrar_matriz(int total_nodos);

        /* Función para visualizar un grafo */
        void crear_grafo(int total_nodos, string nombres[]);

        /* Función para buscar el indice del nodo */
        int buscar_matriz(int total_nodos, string dato, string nombres[]);

        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
        //::::::::  Implimentación del código de apoyo <<conversión C a C++>>:::::::://
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://

        void dijkstra (int total_nodos, string dato, string nombres[]);
        void leer_nodos (char vector[], int total_nodos);
        void inicializar_vector_D (int D[], int total_nodos, int directriz);
        void inicializar_vector_caracter (char vector[], int total_nodos);
        void aplicar_dijkstra (char V[], char S[], char VS[], int D[], int total_nodos, int directriz, string nombres[]);
        void actualizar_VS(char V[], char S[], char VS[], int total_nodos);
        int buscar_indice_caracter(char V[], char caracter, int total_nodos);
        void agrega_vertice_a_S(char S[], char vertice, int total_nodos);
        int elegir_vertice(char VS[], int D[], char V[], int total_nodos);
        void actualizar_pesos (int D[], char VS[], char V[], char v, int total_nodos);
        int calcular_minimo(int dw, int dv, int mvw);
        void imprimir_vector_caracter(char vector[], char *, int total_nodos);
        void imprimir_vector_entero(int vector[], int total_nodos);
        int busca_caracter(char c, char vector[], int total_nodos);

};
#endif