digraph G {
graph [rankdir=LR]
node [style=filled fillcolor=yellow];
a->b [label=4];
a->c [label=11];
b->d [label=6];
b->e [label=2];
c->b [label=3];
c->d [label=6];
e->c [label=5];
e->d [label=3];
}
